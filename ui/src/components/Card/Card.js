import { StyledCard, Body, Footer, Title } from './Card.styles'

const Card = StyledCard
Card.Body = Body
Card.Footer = Footer
Card.Title = Title

export default Card
